from random import randint, choice

valid = []
invalid = []
for i in xrange(256):
    tobin = [x for x in xrange(8)]
    itmp = i
    for j in xrange(8):
        tobin[7-j] = itmp%2
        itmp/=2
    count = 0
    for j in xrange(8):
        count+=tobin[j]
    if count%2 == 1: valid.append(i)
    else: invalid.append(i)

def writeData(f, n, opt, txt, key):
    fstr = str(n)+" "+opt+" "
    for i in xrange(8):
        if i==0: fstr+="{"
        if i!=0: fstr+=","
        fstr+=str(txt[i])
        if(i==7): fstr+="}"
    for i in xrange(8):
        if i==0: fstr+="{"
        if i!=0: fstr+=","
        fstr+=str(key[i])
        if(i==7): fstr+="}"
    fstr+="\n"
    f.write(fstr)

with open("input.txt","w") as f:
    writeData(f, 1, "encrypt", [245,158,90,44,127,167,117,158], [21,191,115,97,162,98,253,87])
    writeData(f, 2, "encrypt", [18,17,188,86,45,129,167,90], [244,158,118,227,61,32,59,73])
    writeData(f, 3, "decrypt", [213,142,103,72,234,56,17,249], [207,213,70,133,2,44,32,87])
    writeData(f, 4, "encrypt", [138,73,64,26,73,8,212,106], [186,31,21,61,25,167,234,214])
    writeData(f, 5, "encrypt", [0, 0, 0, 0, 0, 0, 0, 0], [1, 1, 1, 1, 1, 1, 1, 1])
    writeData(f, 6, "encrypt", [255, 255, 255, 255, 255, 255, 255, 255], [1, 1, 1, 1, 1, 1, 1, 1])
    writeData(f, 7, "encrypt", [255, 255, 255, 255, 255, 255, 255, 255], [2, 2, 2, 2, 2, 2, 2, 2])
    writeData(f, 1, "encrypt", [254, 254, 254, 254, 254, 254, 254, 254], [254, 254, 254, 254, 254, 254, 254, 254])
    for i in xrange(9, 10001):
        txt = []
        key = []
        ch = choice([valid, invalid])
        for j in xrange(8):
            txt.append(randint(0, 256))
            key.append(choice(ch))
        writeData(f, i, choice(["encrypt", "decrypt"]), txt, key )
