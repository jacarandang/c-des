IPShift = [57,49,41,33,25,17,9,1,59,51,43,35,27,19,11,3,61,53,45,37,29,21,13,5,63,55,47,39,31,23,15,7,56,48,40,32,24,16,8,0,58,50,42,34,26,18,10,2,60,52,44,36,28,20,12,4,62,54,46,38,30,22,14,6]
PC_1 = [56,48,40,32,24,16,8,0,57,49,41,33,25,17,9,1,58,50,42,34,26,18,10,2,59,51,43,35,62,54,46,38,30,22,14,6,61,53,45,37,29,21,13,5,60,52,44,36,28,20,12,4,27,19,11,3]
nShift = [1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1]
PC_2 = [13,16,10,23,0,4,2,27,14,5,20,9,22,18,11,3,25,7,15,6,26,19,12,1,40,51,30,36,46,54,29,39,50,44,32,47,43,48,38,55,33,52,45,41,49,35,28,31]
E = [31,0,1,2,3,4,3,4,5,6,7,8,7,8,9,10,11,12,11,12,13,14,15,16,15,16,17,18,19,20,19,20,21,22,23,24,23,24,25,26,27,28,27,28,29,30,31,0]
S1 = [[14,4,13,1,2,15,11,8,3,10,6,12,5,9,0,7] , [0,15,7,4,14,2,13,1,10,6,12,11,9,5,3,8] , [4,1,14,8,13,6,2,11,15,12,9,7,3,10,5,0] , [15,12,8,2,4,9,1,7,5,11,3,14,10,0,6,13]]
S2 = [[15,1,8,14,6,11,3,4,9,7,2,13,12,0,5,10] , [3,13,4,7,15,2,8,14,12,0,1,10,6,9,11,5] , [0,14,7,11,10,4,13,1,5,8,12,6,9,3,2,15] , [13,8,10,1,3,15,4,2,11,6,7,12,0,5,14,9]]
S3 = [[10,0,9,14,6,3,15,5,1,13,12,7,11,4,2,8] , [13,7,0,9,3,4,6,10,2,8,5,14,12,11,15,1] , [13,6,4,9,8,15,3,0,11,1,2,12,5,10,14,7] , [1,10,13,0,6,9,8,7,4,15,14,3,11,5,2,12]]
S4 = [[7,13,14,3,0,6,9,10,1,2,8,5,11,12,4,15] , [13,8,11,5,6,15,0,3,4,7,2,12,1,10,14,9] , [10,6,9,0,12,11,7,13,15,1,3,14,5,2,8,4] , [3,15,0,6,10,1,13,8,9,4,5,11,12,7,2,14]]
S5 = [[2,12,4,1,7,10,11,6,8,5,3,15,13,0,14,9] , [14,11,2,12,4,7,13,1,5,0,15,10,3,9,8,6] , [4,2,1,11,10,13,7,8,15,9,12,5,6,3,0,14] , [11,8,12,7,1,14,2,13,6,15,0,9,10,4,5,3]]
S6 = [[12,1,10,15,9,2,6,8,0,13,3,4,14,7,5,11] , [10,15,4,2,7,12,9,5,6,1,13,14,0,11,3,8] , [9,14,15,5,2,8,12,3,7,0,4,10,1,13,11,6] , [4,3,2,12,9,5,15,10,11,14,1,7,6,0,8,13]]
S7 = [[4,11,2,14,15,0,8,13,3,12,9,7,5,10,6,1] , [13,0,11,7,4,9,1,10,14,3,5,12,2,15,8,6] , [1,4,11,13,12,3,7,14,10,15,6,8,0,5,9,2] , [6,11,13,8,1,4,10,7,9,5,0,15,14,2,3,12]]
S8 = [[13,2,8,4,6,15,11,1,10,9,3,14,5,0,12,7] , [1,15,13,8,10,3,7,4,12,5,6,11,0,14,9,2] , [7,11,4,1,9,12,14,2,0,6,10,13,15,3,5,8] , [2,1,14,7,4,10,8,13,15,12,9,0,3,5,6,11]]
P = [15,6,19,20,28,11,27,16,0,14,22,25,4,17,30,9,1,7,23,13,31,26,2,8,18,12,29,5,21,10,3,24]
IP_1 = [39,7,47,15,55,23,63,31,38,6,46,14,54,22,62,30,37,5,45,13,53,21,61,29,36,4,44,12,52,20,60,28,35,3,43,11,51,19,59,27,34,2,42,10,50,18,58,26,33,1,41,9,49,17,57,25,32,0,40,8,48,16,56,24]

def toBinary(n, ln):
    arr = [x for x in xrange(ln)]
    for i in xrange(ln, 0, -1):
        arr[i-1] = (n%2)
        n/=2
    return arr

def toInteger(arr):
    ans = 0
    mult = 1
    for i in xrange(len(arr), 0, -1):
        ans += arr[i-1]*mult
        mult*=2
    return ans

def XOR(arr1, arr2):
    ans = []
    for i in xrange(len(arr1)):
        ans.append(arr1[i]^arr2[i])
    return ans

def S(arr, n):
    n1 = 2*arr[0]+arr[5]
    n2 = 8*arr[1]+4*arr[2]+2*arr[3]+arr[4]
    if(n == 0): return toBinary(S1[n1][n2], 4)
    if(n == 1): return toBinary(S2[n1][n2], 4)
    if(n == 2): return toBinary(S3[n1][n2], 4)
    if(n == 3): return toBinary(S4[n1][n2], 4)
    if(n == 4): return toBinary(S5[n1][n2], 4)
    if(n == 5): return toBinary(S6[n1][n2], 4)
    if(n == 6): return toBinary(S7[n1][n2], 4)
    if(n == 7): return toBinary(S8[n1][n2], 4)

def IP(arr):
    ans = []
    for i in xrange(64):
        ans.append(arr[IPShift[i]])
    return ans

def getKeys(key):
    keys = []
    keyN = []
    for i in xrange(56):
        keyN.append(key[PC_1[i]])
    C = keyN[0:28]
    D = keyN[28:56]
    for i in xrange(16):
        n = nShift[i]
        C = C[n:28]+C[0:n]
        D = D[n:28]+D[0:n]
        tmpKey = []
        tmp = C+D
        for j in xrange(48):
            tmpKey.append(tmp[PC_2[j]])
        keys.append(tmpKey)
    return keys

def f(r, key):
    rN = []
    for i in xrange(48):
        rN.append(r[E[i]])
    rN = XOR(rN, key)
    ans = []
    for i in xrange(8):
        ans += S(rN[i*6:(i*6+6)], i)
    fans = []
    for i in xrange(32):
        fans.append(ans[P[i]])
    return fans

def encrypt(arr, key, opt):
    #arr = [0,0,0,0, 0,0,0,1, 0,0,1,0, 0,0,1,1, 0,1,0,0, 0,1,0,1, 0,1,1,0, 0,1,1,1, 1,0,0,0, 1,0,0,1, 1,0,1,0, 1,0,1,1, 1,1,0,0, 1,1,0,1, 1,1,1,0, 1,1,1,1]
    #key = [0,0,0,1,0,0,1,1, 0,0,1,1,0,1,0,0, 0,1,0,1,0,1,1,1, 0,1,1,1,1,0,0,1, 1,0,0,1,1,0,1,1, 1,0,1,1,1,1,0,0, 1,1,0,1,1,1,1,1, 1,1,1,1,0,0,0,1]

    arr_1 = IP(arr)
    keys = getKeys(key)

    for i in xrange(16):
        left = arr_1[0:32]
        right = arr_1[32:64]
        tmpArr = right[:]
        curkey = 0
        if opt == "encrypt": curkey = keys[i]
        elif opt == "decrypt": curkey = keys[15-i]
        fout = f(right, curkey)
        tmpArr += XOR(left, fout)
        arr_1 = tmpArr[:]
    arr_1 = arr_1[32:64]+arr_1[0:32]
    fans = []
    for i in xrange(64):
        fans.append(arr_1[IP_1[i]])
    return fans

def checkKey(key):
    for i in xrange(8):
        count = 0
        for j in xrange(8):
            count+=key[i*8+j]
        if count%2 == 0: return False
    return True

if __name__ == '__main__':
    inputs = []
    with open("input.txt", "r") as finput:
        inputs = finput.readlines()
    with open("output.txt", "w") as foutput:
        for inp in inputs:
            fstr = ""
            inps = inp.split()
            fstr+=inps[0]
            nums = map(int, inps[2][1:len(inps[2])-1].replace("}{",",").split(","))
            txt = nums[0:8]
            key = nums[8:16]
            txtbin = []
            keybin = []
            for i in xrange(8):
                txtbin += toBinary(txt[i], 8)
                keybin += toBinary(key[i], 8)
            if not checkKey(keybin):
                foutput.write(fstr+"\n")
                continue

            ans = encrypt(txtbin, keybin, inps[1])
            ansint = []
            for i in xrange(8):
                if(i!=0): fstr+=","
                else: fstr+=" "
                ansint.append(toInteger(ans[i*8:i*8+8]))
                fstr+=str(ansint[i])
            foutput.write(fstr+"\n")
