#include<stdio.h>
#include<stdlib.h>

int IPblock[64] = {57,49,41,33,25,17,9,1,59,51,43,35,27,19,11,3,61,53,45,37,29,21,13,5,63,55,47,39,31,23,15,7,56,48,40,32,24,16,8,0,58,50,42,34,26,18,10,2,60,52,44,36,28,20,12,4,62,54,46,38,30,22,14,6};
int PC_1[56] = {56,48,40,32,24,16,8,0,57,49,41,33,25,17,9,1,58,50,42,34,26,18,10,2,59,51,43,35,62,54,46,38,30,22,14,6,61,53,45,37,29,21,13,5,60,52,44,36,28,20,12,4,27,19,11,3};
int nShift[16] = {1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1};
int PC_2[48] = {13,16,10,23,0,4,2,27,14,5,20,9,22,18,11,3,25,7,15,6,26,19,12,1,40,51,30,36,46,54,29,39,50,44,32,47,43,48,38,55,33,52,45,41,49,35,28,31};
int E[48] = {31,0,1,2,3,4,3,4,5,6,7,8,7,8,9,10,11,12,11,12,13,14,15,16,15,16,17,18,19,20,19,20,21,22,23,24,23,24,25,26,27,28,27,28,29,30,31,0};
int S1[4][16] = {{14,4,13,1,2,15,11,8,3,10,6,12,5,9,0,7} , {0,15,7,4,14,2,13,1,10,6,12,11,9,5,3,8} , {4,1,14,8,13,6,2,11,15,12,9,7,3,10,5,0} , {15,12,8,2,4,9,1,7,5,11,3,14,10,0,6,13}};
int S2[4][16] = {{15,1,8,14,6,11,3,4,9,7,2,13,12,0,5,10} , {3,13,4,7,15,2,8,14,12,0,1,10,6,9,11,5} , {0,14,7,11,10,4,13,1,5,8,12,6,9,3,2,15} , {13,8,10,1,3,15,4,2,11,6,7,12,0,5,14,9}};
int S3[4][16] = {{10,0,9,14,6,3,15,5,1,13,12,7,11,4,2,8} , {13,7,0,9,3,4,6,10,2,8,5,14,12,11,15,1} , {13,6,4,9,8,15,3,0,11,1,2,12,5,10,14,7} , {1,10,13,0,6,9,8,7,4,15,14,3,11,5,2,12}};
int S4[4][16] = {{7,13,14,3,0,6,9,10,1,2,8,5,11,12,4,15} , {13,8,11,5,6,15,0,3,4,7,2,12,1,10,14,9} , {10,6,9,0,12,11,7,13,15,1,3,14,5,2,8,4} , {3,15,0,6,10,1,13,8,9,4,5,11,12,7,2,14}};
int S5[4][16] = {{2,12,4,1,7,10,11,6,8,5,3,15,13,0,14,9} , {14,11,2,12,4,7,13,1,5,0,15,10,3,9,8,6} , {4,2,1,11,10,13,7,8,15,9,12,5,6,3,0,14} , {11,8,12,7,1,14,2,13,6,15,0,9,10,4,5,3}};
int S6[4][16] = {{12,1,10,15,9,2,6,8,0,13,3,4,14,7,5,11} , {10,15,4,2,7,12,9,5,6,1,13,14,0,11,3,8} , {9,14,15,5,2,8,12,3,7,0,4,10,1,13,11,6} , {4,3,2,12,9,5,15,10,11,14,1,7,6,0,8,13}};
int S7[4][16] = {{4,11,2,14,15,0,8,13,3,12,9,7,5,10,6,1} , {13,0,11,7,4,9,1,10,14,3,5,12,2,15,8,6} , {1,4,11,13,12,3,7,14,10,15,6,8,0,5,9,2} , {6,11,13,8,1,4,10,7,9,5,0,15,14,2,3,12}};
int S8[4][16] = {{13,2,8,4,6,15,11,1,10,9,3,14,5,0,12,7} , {1,15,13,8,10,3,7,4,12,5,6,11,0,14,9,2} , {7,11,4,1,9,12,14,2,0,6,10,13,15,3,5,8} , {2,1,14,7,4,10,8,13,15,12,9,0,3,5,6,11}};
int P[32] = {15,6,19,20,28,11,27,16,0,14,22,25,4,17,30,9,1,7,23,13,31,26,2,8,18,12,29,5,21,10,3,24};
int IP_1[64] = {39,7,47,15,55,23,63,31,38,6,46,14,54,22,62,30,37,5,45,13,53,21,61,29,36,4,44,12,52,20,60,28,35,3,43,11,51,19,59,27,34,2,42,10,50,18,58,26,33,1,41,9,49,17,57,25,32,0,40,8,48,16,56,24};

void toBinary(int num, int* bin, int n){
	int i = n-1;
	while(i >= 0){
		bin[i] = num%2;
		num/=2;
		i--;
	}
}

int toInteger(int* bin, int len){
	int i = len-1;
	int ret = 0;
	int mult = 1;
	for(i = len-1; i >=0; i--){
		ret+=bin[i]*mult;
		mult*=2;
	}
	return ret;
}

void leftShift(int* arr, int len, int n){
	int tmp[len];
	int i = 0;
	for(i = 0; i < len; i++){
		tmp[i] = arr[(i+n)%len];
	}
	for(i = 0; i < len; i++) arr[i] = tmp[i];
}

void XOR(int* a, int* b, int len, int* ans){
	int i = 0;
	for(i = 0; i < len; i++){
		ans[i] = a[i]^b[i];
	}
}

void S(int* arr, int* ans, int n){
	int n1 = 2*arr[0] + arr[5];
	int n2 = 8*arr[1] + 4*arr[2] + 2*arr[3] + arr[4];
	if(n == 1) toBinary(S1[n1][n2], ans, 4);
	else if(n == 2) toBinary(S2[n1][n2], ans, 4);
	else if(n == 3) toBinary(S3[n1][n2], ans, 4);
	else if(n == 4) toBinary(S4[n1][n2], ans, 4);
	else if(n == 5) toBinary(S5[n1][n2], ans, 4);
	else if(n == 6) toBinary(S6[n1][n2], ans, 4);
	else if(n == 7) toBinary(S7[n1][n2], ans, 4);
	else if(n == 8) toBinary(S8[n1][n2], ans, 4);

}

void IP(int* arr){
	int Narr[64];
	int i = 0;
	for(i = 0; i < 64; i++){
		Narr[i] = arr[IPblock[i]];
	}
	for(i = 0; i < 64; i++){
		arr[i] = Narr[i];
	}
}

void getKey(int* key, int* K_n){
	int i = 0, j = 0;
	int K_p[56];
	for(i = 0; i < 56; i++) K_p[i] = key[PC_1[i]];
	int *C0 = K_p;
	int *D0 = &K_p[28];

	for(i = 0; i < 16; i++){
		leftShift(C0, 28, nShift[i]);
		leftShift(D0, 28, nShift[i]);

		for(j = 0; j < 48; j++){
		 	K_n[i*48+j] = K_p[PC_2[j]];
		}
	}
}

void f(int* r, int* key, int* ans){
	int nr[48];
	int i = 0;
	for(i = 0; i < 48; i++) nr[i] = r[E[i]];
	int f_ans[48];
	XOR(nr, key, 48, f_ans);
	int f_ans_32[32];
	//S Box
	for(i = 0; i < 8; i++){
		S(&f_ans[i*6], &f_ans_32[i*4], i+1);
	}
	//P Box
	for(i = 0; i < 32; i++) ans[i] = f_ans_32[P[i]];
}

void encrypt(int* arr, int* key, int* cipher){
	int i = 0, j = 0;
	int keys[48*16];
	getKey(key, keys);
	IP(arr);

	//16 rounds
	for(i = 0; i < 16; i++){
		int nxtIter[64];
		int* left = arr;
		int* right = &arr[32];
		//right-to-left
		for(j = 0; j < 32; j++){
			nxtIter[j] = right[j];
		}

		//left-to-right
		int f_out[32];
		int *currentKey = &keys[i*48];
		f(right, currentKey, f_out);
		int f_out_xor_l[32];
		XOR(left, f_out, 32, f_out_xor_l);

		for(j = 0; j < 32; j++) nxtIter[j+32] = f_out_xor_l[j];
		for(j = 0; j < 64; j++) arr[j] = nxtIter[j];

	}
	int inversal[64];
	for(i = 0; i < 64; i++){
		if(i < 32) inversal[i+32] = arr[i];
		else inversal[i-32] = arr[i];
	}
	for(i = 0; i < 64; i++) cipher[i] = inversal[IP_1[i]];
}

void decrypt(int* arr, int* key, int* cleartext){
	//reduce to 1 fxn with encrypt
	int i = 0, j = 0;
	int keys[48*16];
	getKey(key, keys);
	IP(arr);

	//16 rounds
	for(i = 0; i < 16; i++){
		int nxtIter[64];
		int* left = arr;
		int* right = &arr[32];
		//right-to-left
		for(j = 0; j < 32; j++){
			nxtIter[j] = right[j];
		}

		//left-to-right
		int f_out[32];
		int *currentKey = &keys[(15-i)*48];
		f(right, currentKey, f_out);
		int f_out_xor_l[32];
		XOR(left, f_out, 32, f_out_xor_l);

		for(j = 0; j < 32; j++) nxtIter[j+32] = f_out_xor_l[j];
		for(j = 0; j < 64; j++) arr[j] = nxtIter[j];

	}
	int inversal[64];
	for(i = 0; i < 64; i++){
		if(i < 32) inversal[i+32] = arr[i];
		else inversal[i-32] = arr[i];
	}
	for(i = 0; i < 64; i++) cleartext[i] = inversal[IP_1[i]];
}

void getInput(char* inp, int* txt, int* key){
		int tmptxt[8];
		int tmpkey[8];
		sscanf(inp, "{%d,%d,%d,%d,%d,%d,%d,%d}{%d,%d,%d,%d,%d,%d,%d,%d}", &tmptxt[0],&tmptxt[1],&tmptxt[2],&tmptxt[3],&tmptxt[4],&tmptxt[5],&tmptxt[6],&tmptxt[7],&tmpkey[0],&tmpkey[1],&tmpkey[2],&tmpkey[3],&tmpkey[4],&tmpkey[5],&tmpkey[6],&tmpkey[7]);
		int i, j;
		for(i = 0; i < 8; i++){
			toBinary(tmptxt[i], &txt[i*8], 8);
			toBinary(tmpkey[i], &key[i*8], 8);
		}
}

int checkKey(int* key){
	int i, j;
	for(i = 0; i < 8; i++){
		int count = 0;
		for(j = 0; j < 8; j++){
			count+=key[i*8+j];
		}
		if(count%2==0) return 0;
	}
	return 1;
}

int main(){
	/* test code for encrypt and decrypt
	int arr[64] = {0,0,0,0, 0,0,0,1, 0,0,1,0, 0,0,1,1, 0,1,0,0, 0,1,0,1, 0,1,1,0, 0,1,1,1, 1,0,0,0, 1,0,0,1, 1,0,1,0, 1,0,1,1, 1,1,0,0, 1,1,0,1, 1,1,1,0, 1,1,1,1,};
	int key[64] = { 0,0,0,1,0,0,1,1, 0,0,1,1,0,1,0,0, 0,1,0,1,0,1,1,1, 0,1,1,1,1,0,0,1, 1,0,0,1,1,0,1,1, 1,0,1,1,1,1,0,0, 1,1,0,1,1,1,1,1, 1,1,1,1,0,0,0,1, };
	int final[64];
	encrypt(arr, key, final);
	int i;
	for(i = 0; i < 64; i++) fprintf(output, "%d", final[i]);
	fprintf(output, "\n");
	int clear[64];
	decrypt(final, key, clear);
	for(i = 0; i < 64; i++) fprintf(output, "%d", clear[i]);
	*/
	FILE* input = fopen("input.txt", "r");
	FILE* output = fopen("201205175.txt", "w");
	char inp[512];
	int arr[64];
	int key[64];
	int i = 0;
	while(fgets(inp, 200, input)!=NULL){
		int n, i;
		char opt[32];
		char inps[100];
		sscanf(inp, "%d %s %s", &n, opt, inps);
		getInput(inps, arr, key);
		fprintf(output, "%d", n);
		if(!checkKey(key)){
			fprintf(output, "\n");
			continue;
		}

		int out[64];

		if(opt[0] == 'e')
		encrypt(arr, key, out);
		else if(opt[0] == 'd')
		decrypt(arr, key, out);

		int final[8];
		for(i = 0; i < 8; i++){
			if(i==0) fprintf(output, " ");
			final[i] = toInteger(&out[i*8], 8);
			fprintf(output, "%d", final[i]);
			if(i!=7) fprintf(output, ",");
		}
		fprintf(output, "\n");
	}
	fclose(input);
	fclose(output);
}
