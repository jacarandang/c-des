import sys

a = raw_input("Input student number: ")
print("Assuming output file at "+a+".txt")

file = a+".txt"
try:
    f1 = open(file, "r")
    f2 = open("output.txt", "r")
except:
    print("Missing file, ensure your output is in the same directory")
    sys.exit(0)

l1 = f1.readlines()
l2 = f2.readlines()

err = 0
for i in xrange(len(l1)):
    if l1[i] != l2[i]:
        err+=1
        print "diff in line "+str(i+1)
        print "Your Output>>>"
        print l1[i]
        print "--------------"
        print l2[i]
        print "<<< output.txt"

print "Total of "+str(err)+" errors"
